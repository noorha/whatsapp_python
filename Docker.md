# Versions
1.0.5

## Publish docker
docker build -t omnichat/whatsapp-web-api:1.0.0 .
docker push -t omnichat/whatsapp-web-api:1.0.0

## Run docker as a deamon (Devel)
docker run -d \
-e MY_NUMBER='5541995545717' \
-e WEBHOOKS_URL='https://dev-webhooks.omni.chat/whatsapp' \
-e WEBHOOKS_TOKEN='omnichat0987' \
-e WEBSERVER_PORT='5000' \
-e RETAILER_NAME='OmniChat' \
-e PARSE_APP_ID='lHC0oT3G1oZRPB2otwYngjMjKnsOVFGfxWw5PrTM' \
-e PARSE_JAVASCRIPT_KEY='iEBep3E6CcDM1jzJ2pVmgNRpoyGcSJVqH67kN0JA' \
-e PARSE_MASTER_KEY='69iG67YzS0FddY17VmX0Jo2DwK6SjJkg0Gfp3lhO' \
-e PARSE_SERVER_URL='https://dev-private-api.omni.chat/parse' \
-p 5000:5000 \
--name whatsapp-connector-5541988886210 omnichat/whatsapp-web-api:1.0.4

## Run docker as a deamon (Prod)
docker run -d \
-e MY_NUMBER='556299008816' \
-e WEBHOOKS_URL='https://webhooks.omni.chat/whatsapp' \
-e WEBHOOKS_TOKEN='omnichat0987' \
-e WEBSERVER_PORT='5000' \
-e RETAILER_NAME='OmniChat' \
-e PARSE_APP_ID='UCeS99itvZg1tsea2OSoyKvpLbKddhoVAPotIQOy' \
-e PARSE_JAVASCRIPT_KEY='MS0rXGFNWHNtfgd2MqHfQk3W2Uzb4lPhjcMG30fO' \
-e PARSE_MASTER_KEY='UiudZ86foLbJLDMqj5IeSL5edy9fzCuLTcQ4HQbu' \
-e PARSE_SERVER_URL='https://api-private.omni.chat/parse' \
-p 5000:5000 \
--name whatsapp-connector-556299008816 omnichat/whatsapp-web-api:1.0.4