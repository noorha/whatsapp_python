from omnichat.api import start

print('Starting API...')

while True:
    try:
        start()
    except Exception as e:
        print('Exception %s' % e)
        pass
