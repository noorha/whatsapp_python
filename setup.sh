RED='\033[1;31m'
ORANGE='\033[1;33m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

install_requirements() {
    requirements="requirements.txt"
    
    #printf "pip3 -qqq install -r $module_requirements"
    pip3 -qqq install -r $requirements
    
    return_code=$?
    if [ $return_code != 0 ]; then
        printf "${RED}Error:[%d]. Try running it with root privilages\n" $return_code
        exit $return_code
    fi
}

# Step 1
install_requirements
