from omnichat.model import Translator
from omnichat.backend import BackendAdapter
from omnichat import slack

import sys
import logging
import threading
import queue
import requests
import os
import time
import random
import subprocess
import json

from flask import Flask, request, jsonify
from selenium.common.exceptions import TimeoutException

from webwhatsapi import WhatsAPIDriver, ChatNotFoundError
from webwhatsapi.objects.message import Message, MediaMessage


QUEUE_MAX_SIZE_WARNING = 5
queue = queue.Queue()

app = Flask('Mac API')

myNumber = os.environ['MY_NUMBER']
webhooks_url = os.environ['WEBHOOKS_URL']
webhooks_token = os.environ['WEBHOOKS_TOKEN']
webserver_port = os.environ['WEBSERVER_PORT']

profile = "/Users/flaviotorres/Library/Application Support/Firefox/Profiles/onignnvw.default"

# Debug only!!
#driver = WhatsAPIDriver(profile=profile, headless=True)
#driver = WhatsAPIDriver(headless=False, loadstyles=True)

driver = WhatsAPIDriver(headless=True)


def start():
    isLogged = False
    while isLogged == False:
        try:
            qrFilePath = driver.get_qr()
            print('QR Code saved at: {}'.format(qrFilePath))
            url = BackendAdapter.uploadImageFile(qrFilePath)
            notify_new_qr_code(url)

            print('Waiting for QR Code scanning...')
            driver.wait_for_login()
            isLogged = True
        except TimeoutException:
            print('Timeout waiting for login, refreshing')
            pass

    start_flask_worker()
    start_check_for_new_messages_worker()

    start_whatsapp_worker()


def notify_new_qr_code(url):
    msg = 'Novo QR Code para Whatsapp:{} , ele permanecerá disponível por 90 segundos: {}'.format(
        myNumber, url)
    slack.notify(msg)


def handle_received_message(message):
    job = ('receive', message)
    queue.put_nowait(job)


def process_send_job(message):

    # if (message.type == 'text'):
    try:
        chat = driver.get_chat_from_phone_number(message.address)
    except ChatNotFoundError:
        chat = driver.create_chat_by_number(message.address)

    chat.send_message(message.body)
    print('[api-process_send_job]: Text Message sent')

    # elif (message.type == 'variant'):
    #     mac.send_image_to(message.path, message.address,
    #                       caption=message.body)
    # elif (message.type == 'file'):
    #     mac.send_message_to(message.body, message.address)
    # elif (message.type == 'order_status'):
    #     mac.send_message_to(message.body, message.address)
    # elif (message.type == 'payment_request'):
    #     mac.send_message_to(message.body, message.address)


def process_recevied_message(whatsapp_message):
    message = Translator.from_whatapp_message(whatsapp_message)
    json_message = message.to_json()
    jsondataasbytes = json_message.encode('utf-8')   # needs to be bytes
    headers = {
        'Content-Type': 'application/json; charset=utf-8',
        'TOKEN': webhooks_token
    }
    try:
        response = requests.post(
            webhooks_url, data=json_message, headers=headers)
        print('[API:process_recevied_message] - Message posted')
    except Exception as e:
        print("[API:process_recevied_message] exception: %s" % e)
        pass


def start_whatsapp_worker():
    while True:
        try:
            operation, message = queue.get()  # blocks until an item is available
            print("[api-start_whatsapp_worker] - Processing jog %s" % operation)
            if operation == 'send':
                process_send_job(message)
            elif operation == 'check_new_messages':
                for contact in driver.get_unread():
                    for message in contact.messages:
                        process_recevied_message(message)

        except Exception as e:
            print("api-start_whatsapp_worker] - WhatsApp Worker error, reason %s" % e)
            pass
        finally:
            queue.task_done()


def start_flask_worker():
    def start_thread():
        while True:
            try:
                print(
                    '[api-start_flask_worker] - Flask server listen at port: ' + webserver_port)
                app.run(host='0.0.0.0', port=int(webserver_port),
                        debug=False, use_reloader=False)
            except Exception as e:
                print("Flask Worker error, reason %s" % e)
                pass

    t1 = threading.Thread(target=start_thread, name="Flask worker")
    t1.daemon = True
    t1.start()


def start_check_for_new_messages_worker():
    def start_thread():
        while True:
            time.sleep(3)
            queue_size = queue.qsize()
            if queue_size < QUEUE_MAX_SIZE_WARNING:
                print(
                    '[api-start_check_for_new_messages_worker]  - Queing operation for checking for more messages')
                job = ('check_new_messages', '')
                queue.put_nowait(job)
            else:
                print(
                    '[api-start_check_for_new_messages_worker] - Too many messages in the queue, waiting....')

    t1 = threading.Thread(target=start_thread, name="Check for new messages")
    t1.daemon = True
    t1.start()


@app.route('/send', methods=['POST'])
# @swag_from('docs/send.yml')
def handle_send_message():
    print('[api-handle_send_message] - POST request received')
    message = Translator.from_json(request.json)
    job = ('send', message)
    queue.put_nowait(job)

    queue_size = queue.qsize()
    if queue_size > QUEUE_MAX_SIZE_WARNING:
        message = "Fila de envio acima do esperado({}) - atual: {}  para number: {} loja: {}"
        print(message)
        slack.notify(message.format(
            QUEUE_MAX_SIZE_WARNING, queue_size, os.environ['MY_NUMBER'], os.environ['RETAILER_NAME']))

    msg = "The message was sucessfully sended to the queue"
    return msg, 200
