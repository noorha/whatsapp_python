import json
import requests


def notify(message):
    webhook_url = 'https://hooks.slack.com/services/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'
    slack_data = {'text': message, 'username': 'Whatsapp-Web-Connector'}
    requests.post(webhook_url, data=json.dumps(slack_data),
                  headers={'Content-Type': 'application/json'})
