import json
import requests
import os
import sys
fptr = sys.stdout

# app_id = os.environ['PARSE_APP_ID']
# rest_key = os.environ['PARSE_JAVASCRIPT_KEY']
# parse_server = os.environ['PARSE_SERVER_URL']
# master_key = os.environ['PARSE_MASTER_KEY']
#
os.environ['MY_NUMBER'] = '201201611733'
os.environ['WEBHOOKS_URL'] = 'https://dev-webhooks.omni.chat/whatsapp'
os.environ['WEBHOOKS_TOKEN'] = 'omnichat0987'
os.environ['WEBSERVER_PORT'] = '5000'
os.environ['RETAILER_NAME'] = 'OmniChat'
os.environ['PARSE_APP_ID'] = 'lHC0oT3G1oZRPB2otwYngjMjKnsOVFGfxWw5PrTM'
os.environ['PARSE_JAVASCRIPT_KEY'] = 'iEBep3E6CcDM1jzJ2pVmgNRpoyGcSJVqH67kN0JA'
os.environ['PARSE_MASTER_KEY'] = '69iG67YzS0FddY17VmX0Jo2DwK6SjJkg0Gfp3lhO'
os.environ['PARSE_SERVER_URL'] = 'http://localhost'

class BackendAdapter(object):

    @staticmethod
    def saveQrCode(path):
        url = BackendAdapter.uploadImageFile(path)

        headers = {
            "X-Parse-Application-Id": app_id,
            "X-Parse-REST-API-Key": rest_key,
            "X-Parse-Master-Key": master_key
        }

        json = {
            'qrCode': url,
            'whatsAppId': address
        }

        # return requests.post(parse_server + '/functions/saveWhatsAppQrCode', headers=headers, json=json)

    @staticmethod
    def uploadImageFile(path):
        headers = {
            "X-Parse-Application-Id": app_id,
            "X-Parse-REST-API-Key": rest_key,
            "Content-Type": "image/jpeg"
        }
        data = open(path, 'rb').read()
        response = requests.post(
            parse_server + '/files/image.jpg', data=data, headers=headers)
        reponseJSON = json.loads(response.text)
        return reponseJSON['url']

    @staticmethod
    def downloadImageUrl(url):
        local_filename = "app/assets/sent/" + url.split('/')[-1]
        # print("[BackendAdapter:downloadImageUrl] -  Saving file at %s" %
        #       local_filename)

        headers = {
            "X-Parse-Application-Id": app_id,
            "X-Parse-REST-API-Key": rest_key,
        }
        response = requests.get(url, headers=headers)
        with open(local_filename, 'wb') as f:
            for chunk in response.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
        return local_filename

    @staticmethod
    def updateChatPicture(address, path):
        url = BackendAdapter.uploadImageFile(path)

        headers = {
            "X-Parse-Application-Id": app_id,
            "X-Parse-REST-API-Key": rest_key,
            "X-Parse-Master-Key": master_key
        }

        json = {
            'pictureUrl': url,
            'whatsAppAddress': address
        }

        return requests.post(parse_server + '/functions/updateWhatsAppProfilePicture', headers=headers, json=json)
