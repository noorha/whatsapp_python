import json
import datetime
import os
from omnichat.backend import BackendAdapter
from webwhatsapi.objects.message import Message, MediaMessage
from omnichat.decrypter import Decrypter

myNumber = os.environ['MY_NUMBER']


class OmniChatMessage(object):
    def __init__(self):
        self.body = None
        self.to = None
        self.from_id = None
        self.from_name = None
        self.time = None
        self.message_id = None
        self.type = None
        self.url = None

    def to_json(self):
        return json.dumps(self.__dict__)


class Translator(object):
    def __init__(self):
        self.body = None
        self.to = None
        self.from_id = None
        self.from_name = None
        self.time = None
        self.message_id = None
        self.type = None
        self.url = None

    @staticmethod
    def from_json(json):
        message = OmniChatMessage()
        message.address = json.get('address')
        if (json.get('type') == 'text'):
            message.body = json.get('body')
            message.type = 'text'
        elif json.get('type') == 'image':
            message.type = 'image'
            # message.path = BackendAdapter.downloadImageUrl(json.get('url'))
            message.body = json.get('body')
        elif json.get('type') == 'variant':
            message.type = 'variant'
            # message.path = BackendAdapter.downloadImageUrl(json.get('url'))
            message.body = json.get('body')
        elif (json.get('type') == 'file'):
            message.body = json.get('body')
            message.type = 'file'
        elif (json.get('type') == 'order_status'):
            message.body = json.get('body')
            message.type = 'order_status'
        elif (json.get('type') == 'payment_request'):
            message.body = json.get('body')
            message.type = 'payment_request'

        return message

    @staticmethod
    def from_whatapp_message(whatsapp_message):
        message = OmniChatMessage()
        formattedDate = whatsapp_message.timestamp.strftime(
            '%Y-%m-%d %H:%M:%S')
        message.from_id = whatsapp_message.sender.id.split('@')[0]
        message.from_name = whatsapp_message.sender.name
        message.from_profile_image = whatsapp_message.sender.profile_image
        message.to = myNumber
        message.time = formattedDate
        message.message_id = whatsapp_message.id

        if isinstance(whatsapp_message, MediaMessage):
            if whatsapp_message.type == 'ptt':
                message.type = 'audio'
            elif whatsapp_message.type == 'image':
                message.type = 'image'
            elif whatsapp_message.type == 'video':
                message.type = 'video'

            path = '.'
            filePath = path + '/' + whatsapp_message.filename

            decrypter = Decrypter()
            decrypter.getMediaContent(whatsapp_message.clientUrl,
                                      whatsapp_message.mediaKey, whatsapp_message.type)
            decrypter.salvar(filePath)

            # whatsapp_message.save_media(path)
            message.url = BackendAdapter.uploadImageFile(filePath)
            os.remove(filePath)

        elif isinstance(whatsapp_message, Message):
            message.type = 'text'
            message.body = whatsapp_message.content

        # elif whatsapp_message.message_entity._type == 'media':
        #     if whatsapp_message.message_entity.mediaType == 'vcard':
        #         message.type = 'text'
        #         message.body = whatsapp_message.text
        #     elif whatsapp_message.message_entity.mediaType == 'image':
        #         message.type = 'image'

        #         # upload and get the url for the file
        #         message.url = BackendAdapter.uploadImageFile(
        #             whatsapp_message.file_path)

        #         # remove the file after we are done with it
        #         os.remove(whatsapp_message.file_path)

        return message
