FROM ubuntu
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y wget firefox python3 python3-pip

COPY . /usr/src/app
WORKDIR /usr/src/app

# COPY requirements.txt /usr/src/app/
# RUN pip install --no-cache-dir -r requirements.txt

RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.19.1/geckodriver-v0.19.1-linux64.tar.gz
RUN tar -xvzf geckodriver-v0.19.1-linux64.tar.gz
RUN chmod +x geckodriver
RUN cp geckodriver /usr/local/bin/

RUN ./setup.sh

CMD [ "python3", "-u", "./run.py" ]